import os
import shutil
import sys

import exifread
import datetime
from enum import Enum


class FileType(Enum):
    TRASH = 1
    VIDEO = 2
    PHOTO = 3


class FolderStructure(Enum):
    YYYY = 1
    YYYY_MM = 2
    YYYY_MM_DD = 3
    MM = 5
    MM_DD = 6
    DD = 7
    DD_MM = 8
    YYYY_DD_MM = 9
    DD_MM_YYYY = 10
    MM_DD_YYYY = 11

    @classmethod
    def exists(cls, name):
        return name in FolderStructure.__members__


def read_date(image_path):
    try:
        with open(image_path, 'rb') as file:
            tags = exifread.process_file(file)
            try:
                # return Image Taken Time
                return str(tags["EXIF DateTimeOriginal"])
            except KeyError:
                # if cant find Image Taken Time
                return None
    except IsADirectoryError:
        return None
    except PermissionError:
        return None


def check_file_exist(path, file_name):
    if os.path.isfile(path + os.sep + file_name):
        return True
    else:
        return False


def move(old_path, new_path, file_name, overwrite):
    output_file_name = file_name
    if not overwrite:
        while check_file_exist(new_path, output_file_name):
            extension = get_file_extension(output_file_name)
            name = get_file_name(output_file_name)
            name += "n"
            output_file_name = name + extension
    try:
        shutil.move(old_path + os.sep + file_name, new_path + os.sep + output_file_name)
    except FileNotFoundError:
        # if directory doesnt exist - create directory and start again
        os.makedirs(new_path)
        move(old_path, new_path, file_name,overwrite)


def move_to(file_path, new_path, overwrite):
    old_path, file_name = os.path.split(file_path)

    move(old_path, new_path, file_name, overwrite)


def save_log(message,log_path:str):
    log_file_path = log_path + os.sep + "photo_organizer.log"
    # open with cursor in after last line
    with open(log_file_path, 'a') as log_file:
        log_file.write(str(datetime.datetime.now()) + ": " + message + "\n")


def get_argument(arg_prefix: str,source=sys.argv):
    for index, arg in enumerate(source):
        if arg_prefix == arg:
            if index + 1 < len(source):
                arg_value = source[index + 1]
            else:
                return None
            if arg_value[0] == "-":
                return None
            return arg_value
    raise ValueError("No argument with selected prefix")


def get_file_extension(path):
    extension = os.path.splitext(path)[1].lower()
    return str(extension)


def get_file_name(path):
    return os.path.splitext(path)[0]


def get_file_type(extension: str):
    if extension == 'png' or \
            extension == 'jpg' or \
            extension == 'gif' or \
            extension == 'jpeg' or \
            extension == 'heif' or \
            extension == 'heic':
        return FileType.PHOTO
    elif extension == 'mov' or \
            extension == 'mp4' or \
            extension == 'm4v' or \
            extension == 'wmv' or \
            extension == 'mpg' or \
            extension == 'avi':
        return FileType.VIDEO
    elif extension == 'xmp' or \
            extension == 'json' or \
            extension == 'info' or \
            extension == 'db' or \
            extension == 'lnk' or \
            extension == 'xml' or \
            extension == 'tmp' or \
            extension == 'ini' or \
            extension == 'xcf' or \
            extension == 'nri' or \
            extension == 'mswmm':
        return FileType.TRASH
