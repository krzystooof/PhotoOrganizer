import unittest

from main_support import *


class TestSupport(unittest.TestCase):

    def test_get_file_type(self):
        self.assertEqual(get_file_type('png'), FileType.PHOTO)
        self.assertEqual(get_file_type('heic'), FileType.PHOTO)
        self.assertEqual(get_file_type('jpeg'), FileType.PHOTO)

        self.assertEqual(get_file_type('mov'), FileType.VIDEO)
        self.assertEqual(get_file_type('wmv'), FileType.VIDEO)
        self.assertEqual(get_file_type('avi'), FileType.VIDEO)

        self.assertEqual(get_file_type('xmp'), FileType.TRASH)
        self.assertEqual(get_file_type('lnk'), FileType.TRASH)
        self.assertEqual(get_file_type('mswmm'), FileType.TRASH)

    def test_get_file_name(self):
        self.assertEqual(get_file_name('C:\\Users\\user\\Downloads\\PhotoOrganizer-master\\Dow.zip'),
                         "C:\\Users\\user\\Downloads\\PhotoOrganizer-master\\Dow")
        self.assertEqual(get_file_name('D:\\Users\\XYZ.7z'), "D:\\Users\\XYZ")
        self.assertEqual(get_file_name('E:\\win32\\abc.trash'), "E:\\win32\\abc")

        self.assertEqual(get_file_name('~/Dow.wmv'), "~/Dow")
        self.assertEqual(get_file_name('/home/XYZ.zmp4ip'), "/home/XYZ")
        self.assertEqual(get_file_name('mnt/abc.jpg'), "mnt/abc")

    def test_get_file_extension(self):
        self.assertEqual(get_file_extension('C:\\Users\\user\\Downloads\\PhotoOrganizer-master\\Dow.zip'),
                         ".zip")
        self.assertEqual(get_file_extension('D:\\Users\\XYZ.7Z'), ".7z")
        self.assertEqual(get_file_extension('E:\\win32\\abc.TrAsH'), ".trash")

        self.assertEqual(get_file_extension('~/Dow.Waskdfnlfsf'), ".waskdfnlfsf")
        self.assertEqual(get_file_extension('/home/XYZ.zmp4ip'), ".zmp4ip")
        self.assertEqual(get_file_extension('mnt/abc.jpg'), ".jpg")

    def test_get_argument(self):
        source = "-o /usr/output -i /usr/input --videos --nono".split(" ")
        self.assertEqual(get_argument("-o", source=source), "/usr/output")
        self.assertEqual(get_argument("-i", source=source), "/usr/input")
        self.assertEqual(get_argument("--videos", source=source), None)
        self.assertEqual(get_argument("--nono", source=source), None)
        with self.assertRaises(ValueError):
            get_argument("--no_time")

    def test_check_file_exist(self):
        self.assertEqual(check_file_exist(".", "README.md"), True)
        self.assertEqual(check_file_exist(".", "requirements.txt"), True)
        self.assertEqual(check_file_exist(".", "adsfasd.txt"), False)

    def test_read_date(self):
        self.assertEqual(read_date("test_files/base/no_exif.jpg"), None)
        self.assertEqual(read_date("test_files/base/exif.jpg"), '2004:08:31 19:52:58')
        self.assertEqual(read_date("test_files/base/directory"), None)

    def test_move(self):
        # move first file, do not overwrite
        shutil.copy("test_files/base/exif.jpg", "test_files/test/exif.jpg")
        move("test_files/test/", "test_files/new/", "exif.jpg", False)
        self.assertEqual(check_file_exist("test_files/new/", "exif.jpg"), True)
        self.assertEqual(read_date("test_files/new/exif.jpg"), '2004:08:31 19:52:58')
        # move same file, do not overwrite - should create file with name + 'n'
        shutil.copy("test_files/base/exif.jpg", "test_files/test/exif.jpg")
        move("test_files/test/", "test_files/new/", "exif.jpg", False)
        self.assertEqual(check_file_exist("test_files/new/", "exifn.jpg"), True)
        self.assertEqual(read_date("test_files/new/exifn.jpg"), '2004:08:31 19:52:58')
        # move new file, use same name, overwrite
        shutil.copy("test_files/base/no_exif.jpg", "test_files/test/exif.jpg")
        move("test_files/test/", "test_files/new/", "exif.jpg", True)
        self.assertEqual(check_file_exist("test_files/new/", "exif.jpg"), True)
        self.assertEqual(read_date("test_files/new/exif.jpg"), None)

    def test_move_to(self):
        shutil.copy("test_files/base/exif.jpg", "test_files/test/exif.jpg")
        move_to("test_files/test/exif.jpg", "test_files/new/", False)
        self.assertEqual(check_file_exist("test_files/new/", "exif.jpg"), True)
        self.assertEqual(read_date("test_files/new/exif.jpg"), '2004:08:31 19:52:58')

    def tearDown(self):
        try:
            os.remove("test_files/new/exif.jpg")
            os.remove("test_files/new/exifn.jpg")
            os.removedirs("test_files/new/")
        except FileNotFoundError:
            pass


if __name__ == '__main__':
    unittest.main()
