#!/usr/bin/env python3
import time
from pathlib import Path

from main_support import *


def move_to_output(output_path: str, image_path: str, date_taken: str, folder_structure: FolderStructure, flat: bool,
                   overwrite):
    splitted_date = date_taken.split(':')
    year = splitted_date[0]
    month = splitted_date[1]
    day = splitted_date[2][:2]

    old_path, file_name = os.path.split(image_path)
    structure = str(folder_structure.name).split("_")
    new_path = output_path
    for item in structure:
        if item == "YYYY":
            new_path += os.sep + year
        elif item == "MM":
            new_path += os.sep + month
        elif item == "DD":
            new_path += os.sep + day
    if flat:
        new_path.replace("/", "-")
        new_path.replace("\\", "-")
    move(old_path, new_path, file_name, overwrite)


def create_lock(input_path: str):
    lock_file_path = input_path + os.sep + "photo_organizer.lock"
    try:
        # open for exclusive
        with open(lock_file_path, 'x') as lock_file:
            lock_file.write(str(datetime.datetime.now))
        save_log("Created lock", log_path)
    except FileExistsError:
        print("Directory locked - another process in progress")
        save_log("Directory locked - another process in progress", log_path)
        quit(1)


def delete_lock(input_path: str):
    lock_file_path = input_path + os.sep + "photo_organizer.lock"
    os.remove(lock_file_path)
    save_log("Deleted lock", log_path)


def show_help():
    print("Mandatory arguments:")
    print("-> -i <input directory> ")
    print("-> -o <output directory>")
    print("Optional arguments:")
    print("* videos moving and selecting directory:")
    print("-> --videos (move to default videos directory) or --videos <directory> (move to specified directory)")
    print("* photos without date taken moving and selecting directory:")
    print("-> --notime (move to default videos directory) or --notime <directory> (move to specified directory)")
    print("* files that are not photos or videos moving and selecting directory or removing:")
    print("-> --trash (remove from filesystem) or --trash <directory> (move to specified directory)")
    print("* select folder structure:")
    print("-> --structure <folder_structure> (YYYY_MM_DD or YYYY or DD_MM_YYYY and so on)")
    print("* flat folder structure:")
    print("-> --flat")
    print("* overwriting files with identical name in the catalog:")
    print("-> --overwrite")
    print("* select wait time:")
    print("-> --wait <time in seconds>")

    quit(2)


def move_heic(no_time_data_path: str, output_path: str, overwrite):
    # move heic movies to heic photos directories
    for path in Path(no_time_data_path).rglob('*.HEIC'):
        path = str(path)
        old_path, file_name = os.path.split(path)
        name_without_extension = get_file_name(file_name)
        extension = get_file_extension(file_name).upper()
        serached_name = name_without_extension[:-3]  # movies - <photo file name> + (1)
        # search for file in other directiories, if found move file to this directory
        searched_path = []
        for path2 in Path(output_path).rglob(serached_name + extension):
            searched_path.append(str(path2))
        if len(searched_path) == 1:
            searched_path, container = os.path.split(searched_path[0])
            move_to(path, searched_path, overwrite)


def remove_empty_directories(input_path: str):
    for path in Path(input_path).rglob('*'):
        path = str(path)
        if os.path.isdir(path):
            try:
                os.rmdir(path)
            except OSError:
                # directory not empty
                pass


def move_files(input_path: str, output_path: str, videos_path: str, no_time_data_path: str, trash_path: str,
               folder_structure, flat, overwrite):
    moved = 0
    for path in Path(input_path).rglob('*.*'):
        path = str(path)
        if get_file_extension(path)[1:] != 'lock':
            date_taken = read_date(path)
            print(path + ": " + str(date_taken))
            if date_taken is not None:
                move_to_output(output_path,path, date_taken, folder_structure, flat, overwrite)
                moved += 1
            else:
                extension = get_file_extension(path)[1:]
                if get_file_type(extension) == FileType.PHOTO:
                    if no_time_data_path is not None:
                        move_to(path, no_time_data_path, overwrite)
                        moved += 1
                elif get_file_type(extension) == FileType.VIDEO:
                    if videos_path is not None:
                        move_to(path, videos_path, overwrite)
                        moved += 1
                elif get_file_type(extension) == FileType.TRASH:
                    if trash_path == "nopath":
                        os.remove(path)
                    elif trash_path is None:
                        pass
                    else:
                        move_to(path, videos_path)
    return moved


def get_arguments(source=sys.argv):
    arguments = {}
    try:
        arguments['input_path'] = get_argument("-i", source=source)
    except ValueError:
        print("PLEASE SPECIFY INPUT DIRECTORY")
        show_help()
    try:
        arguments['output_path'] = get_argument("-o", source=source)
    except ValueError:
        print("PLEASE SPECIFY OUTPUT DIRECTORY")
        show_help()
    try:
        arguments['videos_path'] = get_argument("--videos", source=source)
        if arguments['videos_path'] is None:
            arguments['videos_path'] = arguments['output_path'] + os.sep + "videos"
    except ValueError:
        arguments['videos_path'] = None
    try:
        arguments['no_time_data_path'] = get_argument("--notime", source=source)
        if arguments['no_time_data_path'] is None:
            arguments['no_time_data_path'] = arguments['output_path'] + os.sep + "no_time"
    except ValueError:
        arguments['no_time_data_path'] = None
    try:
        arguments['trash_path'] = get_argument("--trash", source=source)
        if arguments['trash_path'] is None:
            arguments['trash_path'] = "nopath"
    except ValueError:
        arguments['trash_path'] = None
    try:
        arguments['folder_structure'] = get_argument("--structure", source=source)
        if arguments['folder_structure'] is None or not FolderStructure.exists(arguments['folder_structure']):
            print("FOLDER STRUCTURE SHOULD BE ONE OF:")
            print(list(FolderStructure))
            show_help()
    except ValueError:
        arguments['folder_structure'] = FolderStructure.YYYY_MM_DD
    try:
        get_argument("--flat", source=source)
        arguments['flat'] = True
    except ValueError:
        arguments['flat'] = False
    try:
        get_argument("--overwrite", source=source)
        arguments['overwrite'] = True
    except ValueError:
        arguments['overwrite'] = False
    try:
        arguments['wait_time'] = get_argument("--wait", source=source)
        if arguments['wait_time'] is None:
            print("PLEASE SPECIFY WAIT TIME")
            show_help()
    except ValueError:
        arguments['wait_time'] = 60
    return arguments


if __name__ == '__main__':
    log_path = str(Path.home())

    while True:
        arguments = get_arguments()
        input_path = arguments['input_path']
        output_path = arguments['output_path']
        overwrite = arguments['overwrite']
        no_time_data_path = arguments['no_time_data_path']
        print(str(time.asctime()) + " Starting. Input: " + input_path + ". Output: " + output_path)

        create_lock(input_path)

        moved = move_files(input_path, output_path, arguments['videos_path'], no_time_data_path,
                           arguments['trash_path'], arguments['folder_structure'], arguments['flat'],
                           overwrite)

        remove_empty_directories(input_path)

        move_heic(no_time_data_path,output_path,overwrite)

        delete_lock(input_path)

        time.sleep(arguments['wait_time'])
