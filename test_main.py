import unittest

from main import *


class TestSupport(unittest.TestCase):
    def test_get_arguments(self):
        source = f"-o {os.sep}usr{os.sep}output -i {os.sep}usr{os.sep}input --videos --flat".split(" ")
        arguments = get_arguments(source)
        self.assertEqual(len(arguments), 9)
        self.assertEqual(arguments['output_path'], f'{os.sep}usr{os.sep}output')
        self.assertEqual(arguments['input_path'], f"{os.sep}usr{os.sep}input")
        self.assertEqual(arguments["videos_path"], f"{os.sep}usr{os.sep}output{os.sep}videos")
        self.assertEqual(arguments["flat"], True)
        self.assertEqual(arguments['no_time_data_path'], None)
        self.assertEqual(arguments['trash_path'], None)
        self.assertEqual(arguments['folder_structure'], FolderStructure.YYYY_MM_DD)
        self.assertEqual(arguments['overwrite'], False)
        self.assertEqual(arguments['wait_time'], 60)

if __name__ == '__main__':
    unittest.main()
